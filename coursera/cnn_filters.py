import tensorflow as tf
print(tf.__version__)
keras = tf.keras
print(keras.__version__)
import numpy as np
print(np.__version__)
from PIL import Image

mnist = tf.keras.datasets.fashion_mnist
(training_images, training_labels), (test_images, test_labels) = mnist.load_data()
print(training_images.shape)
print(test_images.shape)

NUM_OF_CLASSES = 10

training_images = training_images.reshape(60000, 28, 28, 1)
training_images = training_images / 255.0

test_images = test_images.reshape(10000, 28, 28, 1)
test_images = test_images/255.0

def get_test_img_indexes_by_classes(IMAGES_PER_CLASS=11):
    VISUALIZATION_IMAGES_PER_CLASS = 6
    ret = [[] for i in range(NUM_OF_CLASSES)]
    for i, im in enumerate(test_images):
        class_index = test_labels[i]
        if len(ret[class_index]) < VISUALIZATION_IMAGES_PER_CLASS:
            ret[class_index].append(im)
        if all(map(lambda a: len(a) == VISUALIZATION_IMAGES_PER_CLASS, ret)):
            break
    ret = np.asarray(ret)
    print('example_images_by_class.shape', ret.shape)
    return ret

example_images_by_class = get_test_img_indexes_by_classes()

NUMBER_OF_LAYERS_TO_EXAMINE = 4

def combine_in_one_image(images_by_class, to_size=None):
    if to_size is not None:
        df = (to_size[0] - images_by_class.shape[2],
              to_size[1] - images_by_class.shape[3])

        dfper2 = (int(df[0] / 2), int(df[1] / 2))

        pad = ((0, 0), (0, 0),
               (dfper2[0], df[0] - dfper2[0]), (dfper2[1], df[1] - dfper2[1]),
               (0, 0))

        print(to_size)
        print(df)
        print(dfper2)
        print(pad)
        images_by_class = np.pad(images_by_class, pad, 'minimum')

    return np.transpose(images_by_class, (0, 2, 1, 3, 4)).reshape(
        images_by_class.shape[0] * images_by_class.shape[2],
        images_by_class.shape[1] * images_by_class.shape[3])


# TODO. why we need separate actiavation model?
# move activation creation model to constructor?
# weights == get_weights()?



# imgs=get_test_img_indexes_by_classes()
# by_conv_layers = get_activation_layers_values(activation_model, imgs)
#
# NUMBER_OF_CONVOLUTIONS = len(by_conv_layers)
# NUMBER_OF_LAYERS = len(by_conv_layers[0])
#
# f, axarr = plt.subplots(NUMBER_OF_LAYERS-1, NUMBER_OF_CONVOLUTIONS)
# for i in range(NUMBER_OF_CONVOLUTIONS):
#     for j in range(1, NUMBER_OF_LAYERS):
#         axarr[j-1, i].imshow(combine_in_one_image(by_conv_layers[i][j], to_size=(28, 28)))
#
# # ret_for_layer

class MC(keras.callbacks.TensorBoard):
    global_step = 0

    def __init__(self, *a, **kwa):
        super(MC, self).__init__(*a, **kwa)

    def get_activation_layers_values(self):
        activation_model = tf.keras.models.Model(
            inputs=self.model.input,
            outputs=[layer.output for layer in self.model.layers])

        activated_by_class = [activation_model.predict(images_for_class)
                              for images_for_class in example_images_by_class]

        ret = []
        for CONVOLUTION_NO in range(VISUALIZATION_NUMBER_OF_CONVOLUTIONS):
            ret_for_convolution = [example_images_by_class]
            for LAYER_NO in range(0, NUMBER_OF_LAYERS_TO_EXAMINE):
                f2 = np.asarray([a_for_class[LAYER_NO] for a_for_class in activated_by_class])
                ret_for_convolution.append(np.expand_dims(f2[:, :, :, :, CONVOLUTION_NO], axis=-1))
            ret.append(ret_for_convolution)
        return ret

    def on_batch_end(self, batch, logs):
        ws = self.model.weights[0].numpy()
        conv_filter = np.asarray(ws[:, :, :, 7])



        by_conv_layers = self.get_activation_layers_values()

        print('activation layers grouped by layer')

        S = self.global_step

        tf.summary.image("filter", np.asarray([conv_filter + 1]), step=S)
        convs = [np.expand_dims(combine_in_one_image(by_conv_layers[1][l_i]), axis=-1) for l_i in
                 range(5)]
        #         np.pad(a, ((3, 2), (2, 3)), 'constant', constant_values=(0, 0))
        #         convs = [np.expand_dims(combine_in_one_image(by_conv_layers[1][l_i]), axis=-1) for l_i in range(0,2)]
        print('-----------------')
        for c in convs:
            print(c.shape)
        print('-----------------')
        tf.summary.image("conv", convs, step=S)

        self.global_step += 1

        super(MC, self).on_batch_end(batch, logs=logs)


tensorboard_callback = MC(log_dir='./logs', update_freq='batch')

file_writer = tf.summary.create_file_writer("./logs/metrics")
file_writer.set_as_default()

model = tf.keras.models.Sequential([
  tf.keras.layers.Conv2D(64, (3, 3), activation='relu', input_shape=(28, 28, 1)),
  tf.keras.layers.MaxPooling2D(2, 2),
  tf.keras.layers.Conv2D(64, (3, 3), activation='relu'),
  tf.keras.layers.MaxPooling2D(2, 2),
  tf.keras.layers.Flatten(),
  tf.keras.layers.Dense(128, activation='relu'),
  tf.keras.layers.Dense(10, activation='softmax')
])

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
model.summary()
model.fit(training_images, training_labels, epochs=5, callbacks=[tensorboard_callback])
# test_loss = model.evaluate(test_images, test_labels)
