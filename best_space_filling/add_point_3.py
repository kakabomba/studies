from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Slider, Button, RadioButtons
import datetime
import itertools
import logging


class DefaultSettings:

    NEAREST_MAX_DISTANCE = 0.75
    FARTHERMOST_MAX_DISTANCE = 2
    HOW_MANY_ACTIVE = 20
    RANDOM_SEED = 19680801
    DIM = 2
    MEAN = 0
    VARIANCE = 1
    N = 100
    DIVERSITY_FUNCTION = None
    DISTANCE_FUNCTION = None
    POINT_SIZE = 10
    POINTS_COLOR_SIZES_BY_TYPE = {
        'NEAREST_TOO_FAR': ('black', 1 * POINT_SIZE),
        'FARTHERMOST_TOO_FAR': ('gray', 1 * POINT_SIZE),
        'DECLINED': ('green', 1 * POINT_SIZE),
        'INACTIVE': ('blue', 4 * POINT_SIZE),
        'SELECTED': ('red', 8 * POINT_SIZE),
    }

    def l2_norm(self, v1, v2):
        """l2 norm"""
        return np.linalg.norm(v1 - v2)

    def l2_norm2(self, v1, v2):
        """l2**2 norm"""
        return self.l2_norm(v1, v2)**2

    def l2_norm4(self, v1, v2):
        """l2**4 norm"""
        return self.l2_norm(v1, v2)**2

    def distance_matrix(self, vectors):
        return np.asarray([[self.DISTANCE_FUNCTION(v1, v2) for v2 in vectors] for v1 in vectors])

    def max_distances(self, distance_matrix):
        return [max(l) for l in distance_matrix]

    def min_distances(self, distance_matrix):
        return [min(l.tolist()[0:i] + l.tolist()[i+1:]) for i, l in enumerate(distance_matrix)]

    def average_distance(self, distance_matrix):
        return distance_matrix.sum() / 2 / (distance_matrix.shape[0] - 1) ** 2

    def diversity_average_distance(self, vectors):
        """Average distance"""
        d_m = self.distance_matrix(vectors)
        return self.average_distance(d_m)
    setattr(diversity_average_distance, '__short_str__', 'avrg_dist')

    def diversity_average_minimal_distance(self, vectors):
        """Average min distance"""
        d_m = self.distance_matrix(vectors)
        return sum(self.min_distances(d_m))/(d_m.shape[0]-1)
    setattr(diversity_average_distance, '__short_str__', 'avrg_min_dist')

    def diversity_average_distance_by_max_distance(self, vectors):
        """Average distance / max distance"""
        d_m = self.distance_matrix(vectors)
        return self.average_distance(d_m) / max(self.max_distances(d_m))
    setattr(diversity_average_distance_by_max_distance, '__short_str__', 'avrg_dist_by_max')

    def diversity_average_distance_by_average_per_item_distance(self, vectors):
        """Average distance / average (per item) max distance"""
        d_m = self.distance_matrix(vectors)
        return self.average_distance(d_m) / sum(self.max_distances(d_m)) / d_m.shape[0]
    setattr(diversity_average_distance_by_average_per_item_distance, '__short_str__', 'avrg_dist_by_avrg_per_item')

    def __init__(self, **kwargs):
        self.DISTANCE_FUNCTION = self.l2_norm
        self.DIVERSITY_FUNCTION = self.diversity_average_distance
        for k in kwargs:
            setattr(self, k, kwargs[k])

    @staticmethod
    def _func_desc(f, short=False):
        if short:
            return (getattr(f, '__short_str__', None)) or f.__doc__ or f.__name__
        else:
            return f.__doc__ or f.__name__

    def __str__(self):
        return f"NEAREST_MAX_DISTANCE={self.NEAREST_MAX_DISTANCE}\n" \
               f"FARTHERMOST_MAX_DISTANCE={self.FARTHERMOST_MAX_DISTANCE}\n" \
               f"HOW_MANY_ACTIVE={self.HOW_MANY_ACTIVE}\n" \
               f"DISTANCE_FUNCTION={self._func_desc(self.DISTANCE_FUNCTION)}\n" \
               f"DIVERSITY_FUNCTION={self._func_desc(self.DIVERSITY_FUNCTION)}\n" \
               f"SEED={self.RANDOM_SEED}\n" \
               f"MEAN={self.MEAN}\n" \
               f"VARIANCE={self.VARIANCE}\n" \
               f"DIM={self.DIM}"

    def __short_str__(self):
        return f"NEAR={self.NEAREST_MAX_DISTANCE}," \
               f"FAR={self.FARTHERMOST_MAX_DISTANCE}," \
               f"ACT={self.HOW_MANY_ACTIVE}," \
               f"DIST={self._func_desc(self.DISTANCE_FUNCTION, True)}," \
               f"DIV={self._func_desc(self.DIVERSITY_FUNCTION, True)}," \
               f"MEAN={self.MEAN}," \
               f"VAR={self.VARIANCE}," \
               f"DIM={self.DIM}".replace(' ', '_')



def add(sel, atype, p, data=None):
    sel.append({'type': atype, 'data': data or {}, 'coords': p['coords']})



def create_fig(SETTINGS):
    scatters = {}
    fig = plt.figure(figsize=(10, 10))
    plt.xlim(-3, 3)
    plt.ylim(-3, 3)

    ax = fig.add_subplot(111)

    plt.subplots_adjust(left=0.25, bottom=0.25)
    ax.text(-2.9, -2.9, SETTINGS.__str__())

    for k, color_size in SETTINGS.POINTS_COLOR_SIZES_BY_TYPE.items():
        scatters[k] = ax.scatter(None, None, c=color_size[0], s=color_size[1])

    ax_step = plt.axes([0.25, 0.1, 0.65, 0.03], facecolor='lightgoldenrodyellow')
    slider_step = Slider(ax_step, 'Step', 0, SETTINGS.N - 1, valinit=SETTINGS.N - 1, valfmt='%d', valstep=1)
    #     dirty hack to keep global reference to slider
    globals()[f'global_reference_to_slider_slider_{datetime.datetime.now()}'] = slider_step

    return fig, scatters, slider_step


def get_active_from_selected(SETTINGS, selected_points, iteration_when_inactivated_by_selected_index,
                             start_index, active_at_frame=None):
    cur_ind = start_index

    ret = []
    real_indexes = []
    while len(ret) < SETTINGS.HOW_MANY_ACTIVE and cur_ind >= 0:
        if iteration_when_inactivated_by_selected_index[cur_ind] is None or \
                (active_at_frame is not None and iteration_when_inactivated_by_selected_index[cur_ind] > active_at_frame):
            ret.append(selected_points[cur_ind])
            real_indexes.append(cur_ind)
        cur_ind -= 1
    return ret[::-1], real_indexes[::-1]


def consider_point(SETTINGS, selected_points, iteration_when_inactivated_by_selected_index, point):
    active_points, active_points_real_indexes = get_active_from_selected(
        SETTINGS,
        selected_points, iteration_when_inactivated_by_selected_index, len(selected_points)-1)

    if SETTINGS.FARTHERMOST_MAX_DISTANCE and any(SETTINGS.DISTANCE_FUNCTION(sp, point) > SETTINGS.FARTHERMOST_MAX_DISTANCE for sp in active_points):
        return 'FARTHERMOST_TOO_FAR', None

    if len(selected_points) < SETTINGS.HOW_MANY_ACTIVE:
        return 'SELECTED', None

    if SETTINGS.NEAREST_MAX_DISTANCE and all(SETTINGS.DISTANCE_FUNCTION(sp, point) > SETTINGS.NEAREST_MAX_DISTANCE for sp in active_points):
        return 'NEAREST_TOO_FAR', None

    combination_indexes = list(
        itertools.combinations(range(len(active_points)), SETTINGS.HOW_MANY_ACTIVE - 1))
    combination_points = [[active_points[ind] for ind in indexes] + [point] for indexes in
                          combination_indexes]
    combination_diversities = list(map(SETTINGS.DIVERSITY_FUNCTION, combination_points))
    better_diversity_index = np.argmax(combination_diversities)
    existing_diversity = SETTINGS.DIVERSITY_FUNCTION(active_points)

    if combination_diversities[better_diversity_index] > existing_diversity:
        better_indexes = list(combination_indexes)[better_diversity_index]
        obsolete_point_index = list(set(range(SETTINGS.HOW_MANY_ACTIVE)) - set(better_indexes))[0]
        logging.debug('combination_indexes', combination_indexes)
        logging.debug('combination_diversities', combination_diversities)
        logging.debug('better_diversity_index', better_diversity_index)
        logging.debug('existing_diversity', existing_diversity)
        logging.debug('obsolete_point_index', obsolete_point_index)
        return 'SELECTED', active_points_real_indexes[obsolete_point_index]
    else:
        return 'DECLINED', None


def calculate_points(SETTINGS, ks):
    points_by_type = {k: [] for k in ks}
    length_of_point_type_at_step = {k: [] for k in ks}
    iteration_when_inactivated_by_selected_index = []
    np.random.seed(SETTINGS.RANDOM_SEED)

    for i in range(SETTINGS.N):
        if i < 0:
            point = np.random.normal(1, SETTINGS.VARIANCE, SETTINGS.DIM)
        else:
            point = np.random.normal(SETTINGS.MEAN, SETTINGS.VARIANCE, SETTINGS.DIM)
        destination, instead_of_index = consider_point(
            SETTINGS,
            points_by_type['SELECTED'], iteration_when_inactivated_by_selected_index, point)

        points_by_type[destination].append(point)
        if destination == 'SELECTED':
            # print(f'destination={destination}, instead_of_index={instead_of_index}')
            iteration_when_inactivated_by_selected_index.append(None)
            if instead_of_index is not None:
                points_by_type['INACTIVE'].append(points_by_type[destination][instead_of_index])
                iteration_when_inactivated_by_selected_index[instead_of_index] = i

        for d in length_of_point_type_at_step:
            length_of_point_type_at_step[d].append(len(points_by_type[d]))

    # proected_2D_points_by_type = {}
    # cols = [2, 4, 7]  # columns to calculate averages, i.e. 3,5,8
    # b = a[:, cols]  # data of the cols
    # c = b.mean(axis=1)
    # for a_type, points in points_by_type:
    #
    #     proected_2D_points_by_type[a_type] = []

    return points_by_type, length_of_point_type_at_step, iteration_when_inactivated_by_selected_index


def draw(SETTINGS, fig, scatters,
         points_by_type, point_of_type_at_step,
         iteration_when_inactivated_by_selected_index, frame):

    for cause in points_by_type:
        points_to_draw = points_by_type[cause]
        array_len_at_iteration_n = point_of_type_at_step[cause][frame]

        if cause == 'SELECTED':
            points_to_draw, _ = get_active_from_selected(
                SETTINGS,
                points_to_draw, iteration_when_inactivated_by_selected_index,
                array_len_at_iteration_n-1, frame)
        else:
            points_to_draw = points_to_draw[0:array_len_at_iteration_n]

        if points_to_draw and len(points_to_draw):
            scatters[cause].set_offsets(points_to_draw)
        else:
            scatters[cause].set_offsets([-1000, -1000])

    fig.canvas.draw_idle()


def go(SETTINGS=None):

    SETTINGS = SETTINGS or DefaultSettings()

    plt.show()
    fig, scatters, slider_step = create_fig(SETTINGS)
    points_by_type, point_of_type_at_step, iteration_when_inactivated_by_selected_index = \
        calculate_points(SETTINGS, [k for k in SETTINGS.POINTS_COLOR_SIZES_BY_TYPE.keys()])
    draw(SETTINGS, fig, scatters, points_by_type, point_of_type_at_step,
         iteration_when_inactivated_by_selected_index, SETTINGS.N-1)
    slider_step.on_changed(lambda val: draw(
        SETTINGS, fig, scatters, points_by_type,
        point_of_type_at_step, iteration_when_inactivated_by_selected_index, int(val)))
    plt.savefig(f'calc_images/{SETTINGS.__short_str__()}.png')
    return points_by_type


if __name__ == '__main__':
    go()
